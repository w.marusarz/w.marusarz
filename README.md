🖥️ Software Developer working with Spring and Kotlin

📚 Recently focused on Data Intensive Applications.

📊Working with data requires some skills in the field of Data Intensive Applications - mostly Kafka, Elasticsearch and MongoDB.
